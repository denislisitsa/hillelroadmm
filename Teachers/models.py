from django.db import models
from faker import Faker

fake = Faker()


class Teacher(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField()
    subject = models.CharField(max_length=100)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)

    @classmethod
    def generate_fake_data(cls):
        for _ in range(10):
            age = fake.random_int(min=25, max=65)
            cls.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                subject=fake.job(),
                age=age,
                email=fake.email(),
                phone_number=fake.phone_number(),
            )

    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.email} {self.age} {self.phone_number} {self.subject}'
