from django.urls import path
from . import views
from .views import create_teacher

urlpatterns = [
    path('generate/', views.list_teachers, name='list_teachers'),
    path('create/', create_teacher, name='create_teacher'),
]

