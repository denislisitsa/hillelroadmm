from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from .models import Teacher


def generate_teachers(request):
    Teacher.generate_fake_data()
    return redirect('list_teachers')


def list_teachers(request):
    teachers = Teacher.objects.all()
    return render(request, 'list_teachers.html', {'teachers': teachers})


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search": fields.Str(required=False)
    },
    location='query'
)
def get_teachers(request, params):
    teachers = Teacher.objects.all()

    search_fields = ['first_name', 'last_name', 'email', 'age', 'subject', 'phone_number']

    for param_name, param_value in params.items():
        if param_name == 'search':
            or_filter = Q()
            for field in search_fields:
                or_filter = or_filter | Q(**{f'{field}__icontains': param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})


@csrf_exempt
def create_teacher(request):
    form = """
    <form method="post">
        <label for="firstNameId">First name:</label><br>
        <input type="text" id="firstNameId" name="first_name" placeholder="First name"><br>

        <label for="lastNameId">Last name:</label><br>
        <input type="text" id="lastNameId" name="last_name" placeholder="Last name"><br><br>

        <label for="emailId">Email address:</label><br>
        <input type="email" id="emailId" name="email" placeholder="Email address"><br><br>
        
        <label for="ageId">Age:</label><br>
        <input type="number" id="ageId" name="age" placeholder="Age"><br><br>
        
        <label for="subjectId">Subject:</label><br>
        <input type="text" id="subjectId" name="subject" placeholder="Subject"><br><br>
        
        <label for="phoneNumberId">Phone Number:</label><br>
        <input type="text" id="phoneNumberId" name="phone_number" placeholder="Phone Number"><br><br>



        <input type="submit" value="Send">
    </form>"""

    if request.method == "POST":
        print(request.POST)
        teacher = Teacher.objects.create(
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'),
            age=request.POST.get('age'),
            subject=request.POST.get('subject'),
            email=request.POST.get('email'),
            phone_number=request.POST.get('phone_number'),
        )
        return HttpResponse(teacher)
    elif request.method == "GET":
        return HttpResponse(form)
    return HttpResponse("Invalid request method.")
