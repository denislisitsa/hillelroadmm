from django.urls import path
from . import views
from .views import create_group

urlpatterns = [
    path('generate/', views.group_list, name='group_list'),
    path('create/', create_group, name='create_group'),
]

