from django.db import models
from faker import Faker

fake = Faker()


class Group(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    location = models.CharField(max_length=100)
    max_capacity = models.PositiveIntegerField()
    leader = models.CharField(max_length=100)
    num_students = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def generate_fake_data(cls):
        for _ in range(5):
            cls.objects.create(
                name=fake.word(),
                description=fake.text(),
                location=fake.city(),
                max_capacity=fake.random_int(min=20, max=50),
                leader=fake.name(),
                num_students=fake.random_int(min=5, max=20),
            )

    def __str__(self):
        return f'{self.name} {self.description} {self.location} {self.max_capacity} {self.leader} {self.num_students}'