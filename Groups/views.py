from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from .models import Group



def generate_groups(request):
    Group.generate_fake_data()
    return render(request, 'Groups/group_list.html')


def group_list(request):
    groups = Group.objects.all()
    return render(request, 'groups/group_list.html', {'groups': groups})

@use_args(
    {"name":fields.Str(required=False),
     "description":fields.Str(required=False),
     "search":fields.Str(required=False),
     },
    location='query',
)
def get_groups(request, params):
    groups = Group.objects.all()

    search_fields = ['name', 'description', 'location']

    for param_name, param_value in params.items():
        if param_name == 'search':
            or_filter = Q()
            for field in search_fields:
                or_filter = or_filter | Q(**{f'{field}__icontains': param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})


@csrf_exempt
def create_group(request):
    form = """
    <form method="post">
        <label for="nameId">Group name:</label><br>
        <input type="text" id="nameId" name="name" placeholder="Group name"><br>

        <label for="descriptionId">Description:</label><br>
        <textarea id="descriptionId" name="description" placeholder="Description"></textarea><br><br>

        <label for="locationId">Location:</label><br>
        <input type="text" id="locationId" name="location" placeholder="Location"><br><br>

        <label for="maxCapacityId">Max Capacity:</label><br>
        <input type="number" id="maxCapacityId" name="max_capacity" placeholder="Max Capacity"><br><br>

        <label for="leaderId">Leader:</label><br>
        <input type="text" id="leaderId" name="leader" placeholder="Leader"><br><br>

        <label for="numStudentsId">Number of Students:</label><br>
        <input type="number" id="numStudentsId" name="num_students" placeholder="Number of Students"><br><br>

        <input type="submit" value="Create">
    </form>"""

    if request.method == "POST":
        print(request.POST)
        group = Group.objects.create(
            name=request.POST.get('name'),
            description=request.POST.get('description'),
            location=request.POST.get('location'),
            max_capacity=request.POST.get('max_capacity'),
            leader=request.POST.get('leader'),
            num_students=request.POST.get('num_students'),
        )
        return HttpResponse(group)

    elif request.method == "GET":
        return HttpResponse(form)

    return HttpResponse("Invalid request method.")


