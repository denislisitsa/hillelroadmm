from django.db import models
from faker import Faker


# Create your models here.
class Student(models.Model):
    first_name = models.CharField(max_length=120, null=True, blank=True)
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=120, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    grade = models.PositiveIntegerField(null=True, blank=True)

    @classmethod
    def generate_instances(cls, count=10):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(),
            )
            

    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.email} {self.birth_date} '