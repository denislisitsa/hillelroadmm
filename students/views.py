from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from .models import Student


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search": fields.Str(required=False),
    },
    location='query'
)
def get_students(request, params):
    students = Student.objects.all()

    search_fields = ['first_name', 'last_name', 'email']

    for param_name, param_value in params.items():
        if param_name == 'search':
            or_filter = Q()
            for field in search_fields:
                or_filter = or_filter | Q(**{f'{field}__icontains': param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    context = {
        'students': students,
        'request': request,
    }

    return render(request, 'students_list.html', context)

@csrf_exempt
def create_student(request):
    form = """
    <form method="post">
        <label for="firstNameId">First name:</label><br>
        <input type="text" id="firstNameId" name="first_name" placeholder="First name"><br>
        
        <label for="lastNameId">Last name:</label><br>
        <input type="text" id="lastNameId" name="last_name" placeholder="Last name"><br><br>
        
        <label for="emailId">Email address:</label><br>
        <input type="email" id="emailId" name="email" placeholder="Email address"><br><br>
        
        <input type="submit" value="Send">
    </form>"""



    if request.method == "POST":
        print(request.POST)
        student = Student.objects.create(
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'),
            email=request.POST.get('email'),
        )
        return  HttpResponseRedirect('/students')
    elif request.method == "GET":
        pass
    return HttpResponse(form)


